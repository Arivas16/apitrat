Apitrat::Application.routes.draw do
  
  get ':controller(/:action(/:id(.:format)))'
  post ':controller(/:action(/:id(.:format)))'

  post 'usuarios/crear', to: 'usuarios#crear'
  post 'usuarios/cambiar_clave', to: 'usuarios#cambiar_clave'
  post 'usuarios/autenticar', to: 'usuarios#autenticar'
  put 'usuarios/editar', to: 'usuarios#editar'
  post 'usuarios/cerrar_sesion', to: 'usuarios#cerrar_sesion'
  delete 'usuarios/eliminar', to: 'usuarios#eliminar'
  get 'usuarios/listar_usuario/:id/:token', to: 'usuarios#listar_usuario'
  post 'usuarios/olvidar_contrasena', to: 'usuarios#olvidar_contrasena'
  post 'tratamientos/crear/:usuario_id/:token/:nombre/:nombre_medico/:fecha_trata/:lunes/:martes/:miercoles/:jueves/:viernes/:sabado/:domingo/:dias/:horas/:hora1/:hora2/:hora3/:hora4/:hora5/:hora6/:hora7/:hora8/:hora9/:hora10/:hora11/:hora12/:hora13/:hora14/:hora15/:hora16/:hora17/:hora18/:hora19/:hora20/:hora21/:hora22/:hora23/:hora24', to: 'tratamientos#crear'
  put 'tratamientos/editar', to: 'tratamientos#editar'
  get 'tratamientos/listar/:id_usuario/:token', to: 'tratamientos#listar'
  get 'tratamientos/listar_recordatorio/:id_usuario/:token', to: 'tratamientos#listar_recordatorio'
  put 'tratamientos/eliminar', to: 'tratamientos#eliminar'
  put 'tratamientos/reprogramar/:id_tratamiento/:id_usuario/:token/:horas/:hora1/:hora2/:hora3/:hora4/:hora5/:hora6/:hora7/:hora8/:hora9/:hora10/:hora11/:hora12/:hora13/:hora14/:hora15/:hora16/:hora17/:hora18/:hora19/:hora20/:hora21/:hora22/:hora23/:hora24', to: 'tratamientos#reprogramar'
  put 'tratamientos/tomar_tratamiento', to: 'tratamientos#tomar_tratamiento'
  get 'tratamientos/notificar', to: 'tratamientos#notificar'
  get 'tratamientos/listar_tratamiento/:id_usuario/:token/:id_tratamiento', to: 'tratamientos#listar_tratamiento' 
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
