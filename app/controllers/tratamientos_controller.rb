class TratamientosController < ApplicationController
ActiveRecord::Base.logger = Logger.new(STDOUT)
  before_filter :set_control_headers
 skip_before_action :verify_authenticity_token

  def set_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, DELETE, PUT, PATCH'
    headers['Access-Control-Allow-Headers'] = '*'
    headers['Access-Control-Request-Method'] = '*'
  end

  #POST /tratamientos/crear/usuario_id/token/lunes/martes/miercoles/jueves/viernes/dias/horas/horas+
  def crear
    @sesion= Sesion.where(usuario_id: params[:usuario_id]).take
    if @sesion!=nil
      if params[:token] == @sesion.token
        if !Tratamiento.where(nombre: params[:nombre], usuario_id: params[:usuario_id]).take
          @tratamiento = Tratamiento.new(id_tratamiento:nil,nombre:params[:nombre],nombre_medico:params[:nombre_medico],fechatrata:params[:fecha_trata],usuario_id:params[:usuario_id], dias: params[:dias], lunes: params[:lunes], martes: params[:martes], miercoles: params[:miercoles], jueves: params[:jueves], viernes: params[:viernes],sabado: params[:sabado], domingo: params[:domingo], contador: 0, contado_at: nil, fecha_ultima_modificacion: nil, borrado: 0)
          n = params[:horas].to_i
           @tratamiento.save
         for i in 1..n
             st= params["hora#{i.to_s}".to_sym]
             if st != ""
               @recordatorio= Recordatorio.new(id_recordatorio: nil, hora: st, tomado: 0, tomado_at: nil, tratamiento_id: @tratamiento.id_tratamiento, borrado: 0)
               @recordatorio.save
            end
          end
          @recordatorios= Recordatorio.where(tratamiento_id: @tratamiento.id_tratamiento).all
          if @tratamiento.save 
             render json: {"nombre" => "Creado"}
          else
            render json:{"nombre" => "Rechazado"}
          end
        else
          render json:{"nombre" => "Nombre existente"}
        end
      else
        render json: {"nombre" => "Rechazado"}
      end
    else
      render json: {"nombre" => "Rechazado"}
    end
  end


  #POST   tratamientos/editar/:id/:usuario_id/:token/:tomado/:dias/:lunes/:martes/:miercoles/:jueves/:viernes/:horas/:hora1
  def editar
    @sesion= Sesion.where(usuario_id: params[:usuario_id]).take
    if @sesion!=nil
       if params[:token] == @sesion.token
          @tratamiento = Tratamiento.find(params[:id])
          if @tratamiento.usuario_id == params[:usuario_id].to_i
            if params[:nombre] == nil || params[:nombre] == ''
              params[:nombre] = @tratamiento.nombre
            end
            if @tratamiento.update(nombre:params[:nombre],nombre_medico:params[:nombre_medico],fechatrata:params[:fecha_trata], dias: params[:dias], lunes: params[:lunes], martes: params[:martes], miercoles: params[:miercoles], jueves: params[:jueves], viernes: params[:viernes],sabado: params[:sabado], domingo: params[:domingo], contador: 0, contado_at: nil, fecha_ultima_modificacion: params[:fecha_ultima_modificacion], borrado: params[:borrado])
              n = params[:horas].to_i
              @recordatorios= Recordatorio.where(tratamiento_id: @tratamiento.id_tratamiento).all
              @recordatorios.each do |recordatorio|
                recordatorio.destroy
              end
              for i in 1..n
                 st= params["hora#{i.to_s}".to_sym]
                 if st!= ""
                    @recordatorio= Recordatorio.new(id_recordatorio: nil, hora: st, tomado: 0, tomado_at: nil, tratamiento_id: @tratamiento.id_tratamiento)
                    @recordatorio.save
                 end
              end
              render json:{"mensaje" => "Actualizado"}
            else
              render json:{"mensaje" => "Rechazado"}
            end
          else
            render json: {"mensaje" => "Rechazado"}
          end
        else
          render json: {"mensaje" => "Rechazado"}
        end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  
  end

  #DELETE  tratamientos/eliminar/:id/:id_usuario/:token
  def eliminar
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @recordatorios= Recordatorio.where(tratamiento_id:params[:id]).all
        @tratamiento = Tratamiento.find(params[:id])
        if @tratamiento.usuario_id == params[:id_usuario].to_i
          @tratamiento.borrado = 1;
          if @tratamiento.save
            @recordatorios.each do |recordatorio|
              recordatorio.borrado= 1;
              recordatorio.save
            end
            render json: {"mensaje" => "Eliminado"}
          else
            render json: {"mensaje" => "Rechazado"}
          end
        else
          render json: {"mensaje" => "Rechazado"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end

  #get /tratamientos/listar/id_usuario
  def listar
    @vacio = []
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @tratamientos = Tratamiento.where(usuario_id:params[:id_usuario]).all
        if @tratamientos
          render json: @tratamientos
        else
          render json: @vacio
        end
      else
        render json: @vacio
      end
    else
      render json: @vacio
    end
  end
 
  #get /tratamientos/lista_recordatorio/id_tratamiento
  def listar_recordatorio
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @tratamientos = Tratamiento.where(usuario_id: params[:id_usuario]).order(id_tratamiento: :asc)
        @respuesta = '['
        @tratamientos.each_with_index do |tratamiento,n|
          @respuesta += '{"id_tratamiento":"'+ tratamiento.id_tratamiento.to_s+'","nombre":"'+tratamiento.nombre+'","nombre_medico":"'+tratamiento.nombre_medico+'","fechatrata":"'+tratamiento.fechatrata.to_s+'","lunes":"'+tratamiento.lunes.to_s+'","martes":"'+tratamiento.martes.to_s+'","miercoles":"'+tratamiento.miercoles.to_s+'","jueves":"'+tratamiento.jueves.to_s+'","viernes":"'+tratamiento.viernes.to_s+'","sabado":"'+tratamiento.sabado.to_s+'","domingo":"'+tratamiento.domingo.to_s+'","dias":"'+tratamiento.dias.to_s+'","fecha_ultima_modificacion":"'+tratamiento.fecha_ultima_modificacion.to_s+'","numero_recordatorio":"'
          @recordatorios = Recordatorio.where(tratamiento_id:tratamiento.id_tratamiento).all
          @respuesta +=  @recordatorios.count.to_s+'","recordatorios":['

          @recordatorios.each_with_index do |recordatorio,i|
            @respuesta+= '{"id_recordatorio":"'+recordatorio.id_recordatorio.to_s+'","hora":"'+recordatorio.hora.to_s+'","tomado":"'+recordatorio.tomado.to_s+'"}'
            if i < (@recordatorios.size() -1)
              @respuesta += ","
            else
              @respuesta += "]"
            end
          end
          if n < (@tratamientos.size() -1)

            @respuesta += "},"
          else
            @respuesta += "}"
          end

        end
        @respuesta += "]"

        
        if @tratamientos && @tratamientos.size()>0
           puts "retorneeee ******************"
          puts @respuesta
          render json: @respuesta
        else
          puts "retorneeee vaciooooo"
          render json: [{"nombre" => "Vacio"}]
        end
      else
        puts "retorneeee rechazado"
        render json: [{"nombre" => "Rechazado"}]
      end
    else
      puts "retorneeee rechazado"
      render json: [{"nombre" => "Rechazado"}]
    end
  end

  def listar_tratamiento
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @tratamiento = Tratamiento.find(params[:id_tratamiento])
          @respuesta = '{"id_tratamiento":"'+ @tratamiento.id_tratamiento.to_s+'","nombre":"'+@tratamiento.nombre+'","nombre_medico":"'+@tratamiento.nombre_medico+'","fechatrata":"'+@tratamiento.fechatrata.to_s+'","lunes":"'+@tratamiento.lunes.to_s+'","martes":"'+@tratamiento.martes.to_s+'","miercoles":"'+@tratamiento.miercoles.to_s+'","jueves":"'+@tratamiento.jueves.to_s+'","viernes":"'+@tratamiento.viernes.to_s+'","sabado":"'+@tratamiento.sabado.to_s+'","domingo":"'+@tratamiento.domingo.to_s+'","dias":"'+@tratamiento.dias.to_s+'","numero_recordatorio":"'
          @recordatorios = Recordatorio.where(tratamiento_id:@tratamiento.id_tratamiento).all
          @respuesta +=  @recordatorios.count.to_s+'","recordatorios":['
          @recordatorios.each_with_index do |recordatorio,i|
            @respuesta+= '{"id_recordatorio":"'+recordatorio.id_recordatorio.to_s+'","hora":"'+recordatorio.hora.to_s+'","tomado":"'+recordatorio.tomado.to_s+'"}'
            if i < (@recordatorios.size() -1)
              @respuesta += ","
            else
              @respuesta += "]"
            end
          end
          @respuesta += "}"

        if @tratamiento 
          render json: @respuesta
        else
          render json: {"mensaje" => "Vacio"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end

#put /tratamientos/tomar_tratamiento
  def tomar_tratamiento
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @recordatorio= Recordatorio.find(params[:id_recordatorio])
        @tratamiento= Tratamiento.find(@recordatorio.tratamiento_id)
        if @tratamiento.usuario_id == params[:id_usuario].to_i
          hora= @recordatorio.hora
          if @recordatorio.update(hora: hora, tomado: true, tomado_at: Time.current)
            render json: {"mensaje" => "Actualizado"}
          else
            render json: {"mensaje" => "Rechazado"}
          end
        else
          render json: {"mensaje" => "Rechazado"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end

#put /tratamientos/:id_tratamiento/:id_usuario/:token/:horas/:hora1/:hora2
  def reprogramar
    @sesion= Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if @sesion.token == params[:token]
        @tratamiento= Tratamiento.find(params[:id_tratamiento])
        if @tratamiento.usuario_id == params[:id_usuario].to_i
          @recordatorios= Recordatorio.where(tratamiento_id: @tratamiento.id_tratamiento).all
          @recordatorios.each do |recordatorio|
            recordatorio.destroy
          end
          n= params[:horas].to_i
          for i in 1..n
                 st= params["hora#{i.to_s}".to_sym]
                 @recordatorio= Recordatorio.new(id_recordatorio: nil, hora: st, tomado: 0, tomado_at: nil,tratamiento_id: @tratamiento.id_tratamiento)
                 @recordatorio.save
          end
          render json:{"mensaje" => "Actualizado"}
        else
          render json: {"mensaje" => "Rechazado"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end


  def notificar
    t = Time.current.to_s.split(' ')
    t1=Time.current
    hora = t[1].split(':')
    @recordatorios= Recordatorio.all
    today = Date.today.strftime("%A").downcase
    notifico= false
    @recordatorios.each do |recordatorio|
      recordar= false
      if recordatorio.tomado_at!=nil
        diatomado= recordatorio.tomado_at.day
        mestomado= recordatorio.tomado_at.month
        anotomado= recordatorio.tomado_at.year
        if t1.year.to_i > anotomado.to_i
          recordatorio.tomado= false
          recordatorio.tomado_at= nil
          recordatorio.save
        elsif t1.year.to_i == anotomado.to_i
          if t1.month.to_i > mestomado.to_i
            recordatorio.tomado= false
            recordatorio.tomado_at= nil
            recordatorio.save
            elsif t1.month.to_i == mestomado.to_i
              if t1.day.to_i > diatomado.to_i
                recordatorio.tomado= false
                recordatorio.tomado_at= nil
                recordatorio.save
              end
          end
        end
      end
      @tratamiento= Tratamiento.find(recordatorio.tratamiento_id)
      if @tratamiento.contador <= @tratamiento.dias
        if @tratamiento.lunes == true && today== "monday"
          recordar= true
        else
          if @tratamiento.martes == true && today== "tuesday"
            recordar= true
          else
            if @tratamiento.miercoles == true && today== "wednesday"
              recordar= true
            else
              if @tratamiento.jueves == true && today== "thursday"
                recordar= true
              else
                if @tratamiento.viernes == true && today== "friday"
                  recordar= true
                else
                  if @tratamiento.sabado == true && today== "saturday"
                    recordar= true
                  else
                    if @tratamiento.domingo == true && today== "sunday"
                      recordar= true
                    end
                  end
                end
              end
            end
          end
        end
        conte= true
        if @tratamiento.contado_at!=nil
          diacontado= @tratamiento.contado_at.day
          mescontado= @tratamiento.contado_at.month
          anocontado= @tratamiento.contado_at.year
          if (t1.year.to_i == anocontado.to_i) && (t1.month.to_i == mescontado.to_i) && (t1.day.to_i == diacontado.to_i)
            conte= false
          end
        end
        if recordar ==true && recordatorio.tomado == false
          notifico=true
          horar= recordatorio.hora.to_s.split(' ')
          horare= horar[1].to_s.split(':')
          if hora[0] > horare[0]
            @usuario= Usuario.find(@tratamiento.usuario_id)
            ActionCorreo.notificar_tratamiento(@usuario.nombre,@usuario.correo,@tratamiento.nombre,horar[1]).deliver
            if conte==true
              cont= @tratamiento.contador + 1
              @tratamiento.contador = cont
              @tratamiento.contado_at = Time.current
              @tratamiento.save
            end
          else
            if hora[0] == horare[0]
              if hora[1] > horare[1]
                @tratamiento= Tratamiento.find(recordatorio.tratamiento_id)
                @usuario= Usuario.find(@tratamiento.usuario_id)
                ActionCorreo.notificar_tratamiento(@usuario.nombre,@usuario.correo,@tratamiento.nombre,horar[1]).deliver
                if conte==true
                  cont= @tratamiento.contador + 1
                  @tratamiento.contador = cont
                  @tratamiento.contado_at = Time.current
                  @tratamiento.save
                end
              else
                if hora[1] == horare[1]
                  if hora[2] > horare[2]
                    @tratamiento= Tratamiento.find(recordatorio.tratamiento_id)
                    @usuario= Usuario.find(@tratamiento.usuario_id)
                    ActionCorreo.notificar_tratamiento(@usuario.nombre,@usuario.correo,@tratamiento.nombre,horar[1]).deliver
                    if conte==true
                      cont= @tratamiento.contador + 1
                      @tratamiento.contador = cont
                      @tratamiento.contado_at = Time.current
                      @tratamiento.save
                    end
                  else
                    if hora[2] == horare[2]
                      @tratamiento= Tratamiento.find(recordatorio.tratamiento_id)
                      @usuario= Usuario.find(@tratamiento.usuario_id)
                      ActionCorreo.notificar_tratamiento(@usuario.nombre,@usuario.correo,@tratamiento.nombre,horar[1]).deliver 
                      if conte==true
                        cont= @tratamiento.contador + 1
                        @tratamiento.contador = cont
                        @tratamiento.contado_at = Time.current
                        @tratamiento.save
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    if notifico== true
      render json:{"mensaje" => "Tratamiento notificado"}
    else
      render json:{"mensaje" => "Tratamiento no notificado"}
    end
  end


end