class UsuariosController < ApplicationController 
  ActiveRecord::Base.logger = Logger.new(STDOUT)
  before_filter :set_control_headers
   skip_before_action :verify_authenticity_token

  def set_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, DELETE, PUT, PATCH'
    headers['Access-Control-Allow-Headers'] = '*'
    headers['Access-Control-Request-Method'] = '*'
  end

  #POST /usuarios/crear
  def crear
   if Usuario.where(correo:params[:correo]).take
      render json:{"nombre" => "Correo existente"}
  else
     @usuario = Usuario.new(id_usuario:nil,nombre:params[:nombre],clave:Usuario.md5(params[:clave]),correo:params[:correo],fechanac:params[:fechanac])
      if @usuario.save
        render json:@usuario
      else
        render json:{"nombre" => "Usuario no creado"}
      end
    end
  end

  def cerrar_sesion
    @sesion = Sesion.where(usuario_id: params[:usuario_id]).take
    if @sesion!=nil
      puts "****************************"
      puts params[:token]
      if params[:token] == @sesion.token
        if @sesion.destroy
          render json: {"mensaje" => "Sesion cerrada"}
        else
          render json: {"mensaje" => "Rechazado"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end

  #POST /usuarios/autenticar/correo/clave
  def autenticar
    @usuario = Usuario.where(correo:params[:correo]).take
    if @usuario != nil
      if @usuario.clave == Usuario.md5(params[:clave]) 
        token = random_alphanumeric(24)
        @sesion = Sesion.where(usuario_id: @usuario.id_usuario).take
        if @sesion == nil
          @sesion= Sesion.new(id_sesion:nil, token: token, usuario_id: @usuario.id_usuario)
          @sesion.save
        else
          @sesion.update(token: token)
        end
        render "usuarios/autenticado.json" 
      else
        render json:{"nombre" => "Error en los datos"}
      end
    else
      render json:{"nombre" => "Error en los datos"}
    end
  end


  #POST /usuarios/editar/id
  def editar
    @sesion = Sesion.where(usuario_id: params[:id]).take
    if @sesion!=nil
      if params[:token] == @sesion.token
        @usuario = Usuario.find(params[:id])
        if params[:nombre] == nil || params[:nombre] == ''
          params[:nombre] = @usuario.nombre
        end
        if params[:correo] == nil || params[:correo] == ''
           params[:correo] = @usuario.correo
        end
    
        @usuario.correo= params[:correo]

        if !Usuario.where(correo: params[:correo]).take
          if @usuario.save
            render json:{"mensaje" => "Datos actualizados"}
          else
            render json:{"mensaje" => "Datos no actualizados"}
          end
        else
           render json:{"mensaje" => "Correo existente"}
        end
      else
        render json:{"mensaje" => "Datos no actualizados"}
      end
    else
      render json:{"mensaje" => "Datos no actualizados"}
    end
  end

  def cambiar_clave
    @sesion = Sesion.where(usuario_id: params[:id]).take
    if @sesion!=nil
      if params[:token] == @sesion.token
        @usuario = Usuario.find(params[:id])
        if @usuario.clave == Usuario.md5(params[:claveact])
          @usuario.clave= Usuario.md5(params[:clavenueva])
          @usuario.save
          render json: {"mensaje" => "Clave modificada"}
        else
          render json: {"mensaje" => "Clave no coincide"}
        end
      else
        render json: {"mensaje" => "Rechazado"}
      end
    else
      render json: {"mensaje" => "Rechazado"}
    end
  end
  

  #DELETE /usuarios/eliminar/id
  def eliminar
    @sesion= Sesion.where(usuario_id: params[:id]).take
    if @sesion!=nil
      if params[:token] == @sesion.token
        @usuario = Usuario.find(params[:id])
        if @usuario.destroy
          render json: {"mensaje" => "Usuario eliminado"}
        else
          render json: {"mensaje" => "Usuario no eliminado"}
        end
      else
        render json: {"mensaje" => "Usuario no eliminado"}
      end
    else
      render json: {"mensaje" => "Usuario no eliminado"}
    end
  end

  #POST /usuarios/olvidar_contrasena/correo
  def olvidar_contrasena
    @usuario = Usuario.where(correo: params[:correo]).take
    if @usuario != nil
      @clave = random_alphanumeric
      @usuario.clave = Usuario.md5(@clave)
      if @usuario.save
        ActionCorreo.recordar_contrasena(@usuario.nombre,@usuario.correo,@clave).deliver
        render json: {"mensaje" => "Contraseña recuperada"}
      else
        render json: {"mensaje" => "Contraseña no recuperada"}
      end
    else
      render json: {"mensaje" => "Correo no existente"}
    end
  end

  #Get /usuarios/listar_usuario/id/token
  def listar_usuario
    @sesion = Sesion.where(usuario_id: params[:id_usuario]).take
    if @sesion!=nil
      if params[:token] == @sesion.token
        @usuario = Usuario.find(params[:id_usuario])
        render json: @usuario
      else
        render json: {"nombre" => "Rechazado"}
      end
    else
      render json: {"nombre" => "Rechazado"}
    end


  end

  private

    def random_alphanumeric(size=8) 
      chars = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a #puede contener letras mayusculas y minusculas, ademas de los numeros
      @clave=(0...size).collect { chars[Kernel.rand(chars.length)] }.join
      return @clave
    end
 
end