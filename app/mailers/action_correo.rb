class ActionCorreo < ActionMailer::Base
  default from: "from@example.com"


 	def recordar_contrasena(nombre,email,clave)
 		@nombre = nombre
 		@email = email
 		@clave = clave
 		mail(to: @email, subject: 'Nueva contraseña')
 	end

 	def notificar_tratamiento(nombre,correo,nombretratamiento,horarecordatorio)
 		@nombre=nombre
 		@correo=correo
 		@nombretrat= nombretratamiento
 		@horarec= horarecordatorio
 		mail(to:@correo, subject: 'Es hora de tomarse sus Medicinas')
 	end

end