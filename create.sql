DROP TABLE IF EXISTS `recordatorio`;
DROP TABLE IF EXISTS `tratamiento`;
DROP TABLE IF EXISTS `sesion`;
DROP TABLE IF EXISTS `usuario`;



CREATE TABLE usuario (
	`id_usuario` INT NOT NULL AUTO_INCREMENT,
	`nombre` varchar(255) NOT NULL,
	`clave` varchar(255) NOT NULL,
	`correo` varchar(255) NOT NULL UNIQUE,
	`fechanac` DATE,
	PRIMARY KEY (`id_usuario`)
);



CREATE TABLE tratamiento (
	`id_tratamiento` int(11) NOT NULL AUTO_INCREMENT,
	`nombre` varchar(255) UNIQUE,
	`nombre_medico` varchar(255),
	`fechatrata` DATE,
	`usuario_id` int NOT NULL,
	`lunes` tinyint(1),
	`martes` tinyint(1),
	`miercoles` tinyint(1),
	`jueves` tinyint(1),
	`viernes` tinyint(1),
	`sabado` tinyint(1),
	`domingo` tinyint(1),
	`dias` int,
	`contador` int,
	`contado_at` DATETIME,
	`fecha_ultima_modificacion` DATETIME,
	`borrado` tinyint(1),
	PRIMARY KEY (`id_tratamiento`),
	KEY `fk_tratamiento_usuario` (`usuario_id`),
  CONSTRAINT `fk_tratamiento_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE

);


CREATE TABLE recordatorio (
	`id_recordatorio` int(11) NOT NULL AUTO_INCREMENT,
	`hora` TIME,
	`tomado` tinyint(1),
	`tomado_at` DATETIME,
	`tratamiento_id` int(11) NOT NULL,
	`borrado` tinyint(1),
	PRIMARY KEY (`id_recordatorio`),
	KEY `fk_recordatorio_tratamiento` (`tratamiento_id`),
    CONSTRAINT `fk_recordatorio_tratamiento` FOREIGN KEY (`tratamiento_id`) REFERENCES `tratamiento` (`id_tratamiento`) ON DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE sesion (
	`id_sesion` int(11) NOT NULL AUTO_INCREMENT,
	`token` varchar(255),
	`usuario_id` int(11) NOT NULL,
	PRIMARY KEY (`id_sesion`),
	KEY `fk_sesion_usuario` (`usuario_id`),
    CONSTRAINT `fk_sesion_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE

);



